package es.olmo.ib3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

public class RootBackupManager implements Serializable,Iterable<BackupManager>{
	private static final long serialVersionUID = 1054490933911459644L;
	public static final String snapExt=".snap";
	public static final String dateFormat="yyMMdd-HHmmss";
	public static final String statName="STAT.gz";
	
	private File rootBackupFile;
	Vector<BackupManager> allBUMs=new Vector<>();
	
	public RootBackupManager(File rbum){
		if(!isDir(rbum,true))
			;
		rootBackupFile=rbum;
	}
	public BackupManager getBUM(String relPath) throws Exception{
		File BUMFile=new File(rootBackupFile,relPath);
		if(!isDir(BUMFile,false))
			return null;
		return new BackupManager(this, relPath);
	}
	public BackupManager createBUM(String relPath) throws Exception{
		File BUMFile=new File(rootBackupFile,relPath);
		BUMFile.mkdirs();
		return new BackupManager(this, relPath);
	}
	private boolean isDir(File rbum, boolean thro){
		if(!rbum.exists() || !rbum.isDirectory()){
			if(thro)
				throw new RuntimeException(rbum.getAbsolutePath()+" Is not a dir or does not exist.");
			else
				return false;
		}
		return true;
	}
	public File getRoot(){
		return rootBackupFile;
	}
	public void log(String txt,String rp){
		log(txt,rp,null);
	}
	public void log(String opt, String rp, String rest[]){
		try {
			File logFile=new File(rootBackupFile,"OpLog.txt");
			FileWriter fw=new FileWriter(logFile, true);
			String date = getCurrentDate();
			fw.write(date);
			fw.write(" ");
			fw.write(opt);
			fw.write(" '");
			fw.write(rp);
			fw.write("'\n");
			if(rest!=null)
				for(String s:rest)
					fw.write(s+"\n");
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static String getCurrentDate() {
		SimpleDateFormat sdf=new SimpleDateFormat(dateFormat);
		String date=sdf.format(new Date().getTime());
		return date;
	}
	public void loadAll(File rbf){
		try {
			File list[]=rbf.listFiles();
			for(File f:list){
				if(!f.isDirectory())
					continue;
				File stat=new File(f,statName);
				if(stat.exists()){
					String pth=f.getAbsolutePath();
					pth=pth.substring(1+rootBackupFile.getAbsolutePath().length());
					allBUMs.add(new BackupManager(this, pth));
				}
				loadAll(f);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			allBUMs=new Vector<>();
		}
	}
	@Override
	public Iterator<BackupManager> iterator() {
		if(allBUMs.size()==0)
			loadAll(rootBackupFile);
		return allBUMs.iterator();
	}
	public static long parseDate(String s){
		long ret=0;
		try {
			SimpleDateFormat sdf=new SimpleDateFormat(RootBackupManager.dateFormat);
			Date d=sdf.parse(s);
			ret=d.getTime();
		}catch(Exception e){
			throw new RuntimeException("Bad date '"+s+"'");
		}
		return ret;
	}	
}
