package es.olmo.ib3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;

import de.schlichtherle.truezip.zip.ZipEntry;
import de.schlichtherle.truezip.zip.ZipFile;
import de.schlichtherle.truezip.zip.ZipOutputStream;

public class Snapshot implements Serializable {
	private static final long serialVersionUID = -2766258539780736578L;
	private transient int created=0,updated=0,deleted=0;
	private String zipRef;
	Nodo root=new Nodo();
	BackupManager bum;
	
	public Snapshot(BackupManager bum){
		this.bum=bum;
	}
	
	public Snapshot(File snpf){
		Progress p=new Progress("Stating files", 0, 0, 80);
		root=new Nodo(snpf,p);
		String nDirs=new DecimalFormat("#,##0").format(root.getNumberOfDirs());
		String nFils=new DecimalFormat("#,##0").format(root.getNumberOfFiles(Nodo.opAll));
		System.out.printf("\nFound %s Dirs and %s files\n",nDirs,nFils);
	}

	public void diff(Snapshot newer){
		created=updated=deleted=0;
		buffer=new byte[4194304];
		//Mark childs as deleted and untouched
		for(Nodo olderChild:this.root)
			olderChild.resetStatus(0,Nodo.opDeleted,false,null);
		for(Nodo newerChild:newer.root)
			newerChild.resetStatus(0,Nodo.opUntouched,false,null);
		System.out.print("Performing diff..");
		zipRef=RootBackupManager.getCurrentDate();
		diff(root,newer.root);
		System.out.printf("%d news, %d updated, %d deleted\n", created,updated,deleted);
	}
	
	public void diff(Nodo esteDir, Nodo otroDir){
		for(Nodo remChild:otroDir){
			long fech=remChild.getFileRef().lastModified();
			Nodo local=esteDir.getChildWithSameName(remChild);
			if(local==null){ //Nuevo
				//Añadimos el nodo al local
				esteDir.addHijo(remChild);
				remChild.resetStatus(fech,Nodo.opCreated,true,zipRef);//Y sus hijos
				created++;
			}else{
				local.resetStatus(0,Nodo.opUntouched,false,null);//Not deleted
				if(local.isDir() && remChild.isDir()){
					diff(local,remChild);
				}else if(local.isDir() || remChild.isDir()){
					//Hmm, suppose local is deleted and created as a file/dir when was dir/file
					local.resetStatus(fech-1,Nodo.opDeleted,true,zipRef);
					deleted++;
					remChild.resetStatus(fech,Nodo.opCreated,true,zipRef);
					created++;
				}else if(local.getDate()<remChild.getDate()){//Updated or re-created
					if(local.isDeleted()){
						local.resetStatus(fech,Nodo.opCreated,true,zipRef);
						local.setTFRef(remChild.getFileRef());
						created++;
					}else{
						local.resetStatus(fech,Nodo.opUpdated,true,zipRef);
						local.setTFRef(remChild.getFileRef());
						updated++;
					}
				}
			}
		}
		//Quedan marcados como opDeleted los nodos que no han sido encontrados
		for(Nodo localChild:esteDir){
			if(localChild.getStatus()==Nodo.opDeleted){
				deleted++;
				localChild.resetStatus(System.currentTimeMillis(),Nodo.opDeleted, true, zipRef);
			}
		}
	}
	
	public void commit(ZipOutputStream zf, Progress p) throws Exception{
		for(Nodo child:root)
			commit(child,zf,p);
		p.finish();
	}
	
	public void commit(Nodo colored, ZipOutputStream zf,Progress p) throws Exception{
		if(colored.isDir()){
			for(Nodo child:colored)
				commit(child,zf,p);
		}else{
			switch(colored.getStatus()){
			case Nodo.opCreated: 
			case Nodo.opUpdated:
				save(colored,zf,p);
				break;
			case Nodo.opDeleted:
				delete(colored,zf,p);
				break;
			}
		}
	}
	
	public void extract(ExtractManager xt) throws Exception{
		for(Nodo child:root)
			extract(child,xt);
		xt.finishExtract();
	}
	
	public transient ZipFile openedZipFile;
	public transient String openedZipFileName="Noopened zip file name s registerd";
	public void extract(Nodo colored, ExtractManager xt) throws Exception{
		if(colored.isDeleted())
			return;
		if(colored.isDir()){
			for(Nodo child:colored)
				extract(child,xt);
		}else{
			String zfn=colored.getSelZip()+RootBackupManager.snapExt;
			if(openedZipFileName==null || !openedZipFileName.equals(zfn)){
				if(openedZipFile!=null)
					openedZipFile.close();
				openedZipFile=new ZipFile(new File(bum.getFileDir(),zfn));
				openedZipFileName=zfn;
			}
			String relPath=colored.getRelativeName();
			xt.itemExtract(
					colored.getRelativeName(),
					openedZipFile.getInputStream(relPath),
					colored.getName());
		}
	}
	
	private static final String delMark="<DELETED>";
	private void delete(Nodo colored, ZipOutputStream zf,Progress prog) throws Exception{
		String relName=colored.getRelativeName();
		ZipEntry ze=new ZipEntry(relName+delMark);
		zf.putNextEntry(ze);
		colored.setDeleted(true);
	}
	
	private transient byte buffer[]=null;
	public void save(Nodo colored, ZipOutputStream zf, Progress prog) throws IOException {
		//start preparing item
		File in;
		FileInputStream fis;
		in = colored.getFileRef();
		try {
			fis = new FileInputStream(in);
		} catch (FileNotFoundException e) {
			System.err.println("\nCannot open "+in.getAbsolutePath()+". It WILL NOT BE Backed up");
			return;
		}

		//prog.startPrepareItem();
		colored.setDeleted(false);
		colored.setDate(in.lastModified());

		String relName=colored.getRelativeName();
		ZipEntry entry=new ZipEntry(relName);
		zf.setLevel(CmpExt.getCmpLevel(relName));
		zf.putNextEntry(entry);
		
		int readed;
		//End prepare item
		//prog.endPrepareItem();
		while((readed=fis.read(buffer))>=0){
			zf.write(buffer, 0, readed);
			//report size advance
			prog.reportAdvance(readed, colored.getFullName());
		}
		fis.close();
	}

	public String getZipRef() {
		return zipRef;
	}

	public static String readableFileSize(long size) {
	    if(size <= 0) return "0";
	    final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    return new DecimalFormat("#,##0.##").format(size/Math.pow(1024, digitGroups)) + units[digitGroups];
	}
	
	public long getTotalSize(byte mask){
		return root.getSize(mask);
	}
	
	public long getTotalNumber(byte mask){
		return root.getNumberOfFiles(mask);
	}
}
