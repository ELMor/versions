package es.olmo.ib3;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Pattern;

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;

import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.zip.ZipOutputStream;

public class BackupManager implements Serializable {
	private static final long serialVersionUID = -2766739780736578L;
	private RootBackupManager rbm;
	private String relPath;
	private Snapshot current=new Snapshot(this);
	
	public BackupManager(RootBackupManager rbm, String rp) throws Exception{
		relPath=rp;
		this.rbm=rbm;
		File dirOfBackup=new File(rbm.getRoot(),relPath);
		File statFile=new File(dirOfBackup,RootBackupManager.statName);
		if(!statFile.exists())
			createSaveAndLoadStat();
		else
			loadStat();
		current.root.setName(relPath);
	}
	
	public Snapshot getCurrentSnapshot(){
		return current;
	}
	
	public void diff(Snapshot newer){
		current.diff(newer);
	}
	
	public void match(String pattern, String dsel){
		current.root.resetStatus(0,Nodo.opUntouched, false, null);
		Pattern pat=substitutePatternChars(pattern);
		current.root.matchWalker(pat,dsel);
	}

	public void showMatched(){
		current.root.showWalkerTree(0);
	}
	
	public static Pattern substitutePatternChars(String pattern) {
		Pattern pat;
		pattern=pattern.replace(".", "\\.");
		pattern=pattern.replace("?", ".");
		pattern=pattern.replace("*", ".*");
		pat=Pattern.compile(pattern);
		return pat;
	}
	
	public void commit() throws Exception{
		//Save the diff
		ZipOutputStream zipped=getNextSnapshot(current.getZipRef());
		Progress p = getProgress("Updating",(byte)(Nodo.opUpdated|Nodo.opCreated));
		//And save
		current.commit(zipped,p);
		zipped.close();
		//And save Stat
		saveStat();
	}

	public void extract(ExtractManager xt) throws Exception{
		current.extract(xt);
	}
	
	private Progress getProgress(String s,byte mask) {
		long szOfItems=current.getTotalSize(mask);
		long numbItems=current.getTotalNumber(mask);
		String msg=String.format(s+" %s",Snapshot.readableFileSize(szOfItems));
		Progress p=new Progress(msg, numbItems, szOfItems, 80);
		return p;
	}
	
	public void createSaveAndLoadStat() throws Exception{
		//LIst all snapshots
		TFile myDir=new TFile(rbm.getRoot(),relPath);
		TFile list[]=myDir.listFiles(new snapshotFilter());
		//Sorts ascending int time, older is first
		Arrays.sort(list,new snapshotSort());
		if(list.length>0){
			current=new Snapshot(list[0]);
			for(int i=1;i<list.length;i++){
				Snapshot other=new Snapshot(list[i]);
				current.diff(other);
			}
		}
		saveStat();
	}
	
	public ZipOutputStream getNextSnapshot(String name) throws Exception{
		File bumFile=new File(rbm.getRoot(),relPath);
		File zfFile=new File(bumFile,name+RootBackupManager.snapExt);
		FileOutputStream fos=new FileOutputStream(zfFile);
		ZipOutputStream zf=new ZipOutputStream(fos);
		return zf;
	}
	
	private void saveStat() throws Exception{
		File stat=new File(new File(rbm.getRoot(),relPath),RootBackupManager.statName);
		FileOutputStream fos=new FileOutputStream(stat);
		GzipCompressorOutputStream gzcos=new GzipCompressorOutputStream(fos);
		ObjectOutputStream oos=new ObjectOutputStream(gzcos);
		oos.writeObject(relPath);
		oos.writeObject(current);
		oos.close();
	}
	
	private void loadStat() throws Exception{
		File stat=new File(new File(rbm.getRoot(),relPath),RootBackupManager.statName);
		FileInputStream fis=new FileInputStream(stat);
		GzipCompressorInputStream gzcis=new GzipCompressorInputStream(fis);
		ObjectInputStream ois=new ObjectInputStream(gzcis);
		relPath=(String)ois.readObject();
		current=(Snapshot)ois.readObject();
		ois.close();
	}

	class snapshotFilter implements FileFilter {
		@Override
		public boolean accept(File f) {
			String n=f.getName();
			if(f.isDirectory())
				return false;
			if(!n.toLowerCase().endsWith(RootBackupManager.snapExt))
				return false;
			return true;
		}
	}
	
	class snapshotSort implements Comparator<File>{
		@Override
		public int compare(File f1, File f2) {
			return (int)(f1.lastModified()-f2.lastModified());
		}
	}
	
	public File getFileDir(){
		return new File(rbm.getRoot(),relPath);
	}
	
	public File getStatFile(){
		return new File(getFileDir(),RootBackupManager.statName);
	}
	
	public String getRelPath(){
		return relPath;
	}
	
}
