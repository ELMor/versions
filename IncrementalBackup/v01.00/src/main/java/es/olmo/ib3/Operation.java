package es.olmo.ib3;

import java.io.File;
import java.io.FileInputStream;
import java.util.Vector;

public class Operation {
	RootBackupManager rbuManager;
	Vector<String> fileSpecs;
	String rootOfSource;
	String dateOfOperation;
	
	public Operation(RootBackupManager rbum, String rSrc, Vector<String> spec) throws Exception{
		rbuManager=rbum;
		fileSpecs=spec;
		rootOfSource=rSrc;
	}
	public void update() throws Exception{
		if(fileSpecs.size()==0){
			File list[]=new File(rootOfSource).listFiles();
			for(File f:list)
				if(f.isDirectory())
					fileSpecs.add(f.getName());
		}
		for(String relPath:fileSpecs){
			System.out.println("\nUpdate on "+relPath+"...");
			BackupManager bum=initBUM("For Update of ", relPath);
			//Build File System snapshot
			Snapshot other=new Snapshot(new File(rootOfSource,relPath));
			bum.diff(other);
			bum.commit();
		}
	}
	public void list() throws Exception{
		for(String relPath:fileSpecs){
			System.out.println("List of "+relPath);
			for(BackupManager bum:rbuManager){
				bum.match(relPath,null);
				System.out.println("  Checking "+bum.getRelPath());
				bum.showMatched();
			}
		}
	}
	/**
	 * Extract files
	 * @param fileDest may be null.
	 * @throws Exception 
	 */
	public void extract(String dateSelected, String fileDest) throws Exception{
		Vector<BackupManager> bumList=new Vector<>();
		if(fileDest==null)
			fileDest=".";
		long totalItems=0;
		long totalSize=0;
		for(String relPath:fileSpecs){
			for(BackupManager bum:rbuManager){
				bum.match(relPath,dateSelected);
				bumList.add(bum);
				Snapshot snap=bum.getCurrentSnapshot();
				totalItems+=snap.getTotalNumber(Nodo.opMatch);
				totalSize+=snap.getTotalSize(Nodo.opMatch);
			}
		}
		Progress p=new Progress("Extracting", totalItems, totalSize, 80);
		ExtractManager xt;
		if(fileDest.toLowerCase().endsWith(".zip")){
			xt=new ZipExtractDestination(p, fileDest);
		}else{
			xt=new FileExtractDestination(new File(fileDest), p);
		}
		for(BackupManager bum:bumList)
			bum.extract(xt);
	}
	
	public void portable(String fileDest) throws Exception{
		if(fileDest==null)
			fileDest="Portable-"+RootBackupManager.getCurrentDate()+".zip";
		Progress prog=new Progress("Creating "+fileDest, 0, 0, 80);
		ExtractManager xt=new ZipExtractDestination(prog, fileDest);
		for(BackupManager bum:rbuManager){
			File stat=bum.getStatFile();
			String rp=bum.getRelPath();
			xt.itemExtract(rp, new FileInputStream(stat), rp);
		}
		xt.finishExtract();
	}
	public void stat() throws Exception{
		for(BackupManager bum:rbuManager){
			File stat=bum.getStatFile();
			stat.delete();
			System.out.println("Stating "+stat.getAbsolutePath());
			bum.createSaveAndLoadStat();
		}
	}
	private BackupManager initBUM(String logTxt, String relPath) throws Exception {
		BackupManager bum=rbuManager.getBUM(relPath);
		if(bum==null){
			bum=rbuManager.createBUM(relPath);
			rbuManager.log("Creating ",relPath);
		}
		chkBUM(bum, rootOfSource);
		rbuManager.log(logTxt,relPath);
		return bum;
	}
	private void chkBUM(BackupManager bum, String relPath){
		if(bum==null)
			throw new RuntimeException(relPath+" is not a backup location.");
	}
}
