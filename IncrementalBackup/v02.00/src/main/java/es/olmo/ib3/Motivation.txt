BI (Backup Incremental)
=======================	
	Is a utility to backup dirs. It's not a new thing, except for one curiosity: 

	I work with a portable PC while traveling, and wants to be able to make a
incremental backup from a big storage on my house. Obviously I can not carry
these big storage with me, so I buid this.

	The first step is choose a dir to backup (say that, f.e. z:\Backup). I 
call it 'BackupDestination'. Then the dir i want to copy (f.e. c:\Users\elinares), 
for that I exec:

	java -jar ib.jar -b z:\Backup -r c:\Users -u elinares

	Note there is 3 names playing: The root of backup (z:\Backup), the root of
source directory (c:\Users) and the directory inside it (elinares). You can 
specify more than one of this directories in the command line. If you want to
backup a whole disk you can do:

	java -jar ib.jar -b z:\Backup -r c:\ -u .
	
	Back to older sample:
	
	java -jar ib.jar -b z:\Backup -r c:\Users -u elinares

	It creates a subdir z:\Backup\Users\elinares, then calculates all files 
under c:\Users\elinares and creates a zip with the current date there: 
	
	z:\Backup\Users\elinares\YYMMDD-HHMMSS.zip

	And an additional one: z:\Backup\Users\elinares\STAT.gz that holds 
attributes of every file zipped (date, size and name). You can maintain the
same DestinationBackup for other backups:
 
	java -jar ib.jar -u -b z:\Backup -r c:\Users second
	java -jar ib.jar -u -b z:\Backup -r c:\Users third

	(or java -jar ib.jar -b z:\Backup -r c:\Users -u second third)

z:\Backup holds then 
		z:\Backup\Users\elinares (YYMMDD-HHMMSS.zip, STAT.gz) 
		z:\Backup\Users\second   (YYMMDD-HHMMSS.zip, STAT.gz)
		z:\Backup\Users\third    (YYMMDD-HHMMSS.zip, STAT.gz)

If I exec again other day "java -jar ib.jar -b z:\Backup -r c:\Users -u elinares" 
another zip is created only with new and modified files in Users dir.

 z:\Backup holds then 
		z:\Backup\Users\elinares (YYMMDD-HHMMSS.zip, YYMMDD-HHMMSS.zip, STAT.gz) 
		z:\Backup\Users\second   (YYMMDD-HHMMSS.zip, STAT.gz)
		z:\Backup\Users\third    (YYMMDD-HHMMSS.zip, STAT.gz)

Those dirs (elinares, second, third) are used for listing. If i wants to see if a
file is backed up (and how many times) I can do:

	java -jar ib.jar -b z:\Backup -l *.mp3
	java -jar ib.jar -b z:\Backup -l elinares

	Other options as 'x' extract from one of the existing zips (and uses the
same way of listing). 's' Is used to restat dirs (I mean: re-creates the 
STAT.gz files from zips on DestinationBackup subdirs).

	One more thing: 'p' option (portable) creates a zip on the current dir 
which holds all STAT.gz of DestinationBackup, which are the unique info you 
need to create a differential backup on an USB key when traveling. When you 
return home, you must copy DestinationBackup from key to big storage in 
house. 