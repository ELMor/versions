package es.olmo.ib3;

import java.text.DecimalFormat;


public class Progress {
	public static final long szUnknown=0;
	public static final long numberUnknown=0;
	private String spaces="                 ";
	private int consoleLineSize;
	private String staticMsg;
	private long currentSize=0;
	private long totalSizeOfItems;
	private long totalNumberOfItems;
	
	public Progress(String txt, long numberOfItems, long szOfItems, int lineSz){
		consoleLineSize=lineSz;
		while(spaces.length()<consoleLineSize)
			spaces+=spaces;
		spaces=spaces.substring(0,consoleLineSize);
		totalSizeOfItems=szOfItems;
		totalNumberOfItems=numberOfItems;
		staticMsg=txt;
	}
	
	public void setMessage(String m){
		staticMsg=m;
	}
	
	public void setMTBP(long minTimeBetweenReps){
		minTimeBetReps=minTimeBetweenReps;
	}
	
	private long currentItem=0;
	private long startPrep=-1;
	private long endPrep=-1;
	private float mediaPrepareItem=0; 
	public void startPrepareItem(){
		startPrep=System.currentTimeMillis();
	}
	
	public void endPrepareItem(){
		endPrep=System.currentTimeMillis();
		mediaPrepareItem = ((float)((endPrep-startPrep)+(mediaPrepareItem*currentItem)))/(++currentItem);
	}
	
	public boolean reportAdvance(long avance,String msg){
		return reportAdvance(avance,msg,"");
	}

	private long initProcess=-1;
	private long lastAdvCall=-1;
	private long minTimeBetReps=500;
	private long estimateInterval=4000;
	private int estimatePos=-1,estimatePosInc=1;
	private final static String points="----";
	public boolean reportAdvance(long avance, String msg, String rightMsg){
		//Ahora
		long now=System.currentTimeMillis();
		if(initProcess==-1)
			initProcess=now;
		long intervalBetweenCalls=now-lastAdvCall;
		long totalInterval=now-initProcess;
		//Registrar avance
		currentSize+=avance;
		//Si es el primer informe inicializamos el tiempode la ultima
		//presentacion
		if( intervalBetweenCalls < minTimeBetReps)
			return false;
		String show="";
		boolean isHalfPercent= (totalSizeOfItems!=0) ?
				currentSize/totalSizeOfItems >0.5f :
				true;
		if( totalInterval < estimateInterval && !isHalfPercent){
			show = estimatingMessage();
		}else{
			if(totalNumberOfItems==szUnknown || mediaPrepareItem==0){	
				show = advanceMessage(now, msg, show, rightMsg);
			}else{
				long intervalOfSize=totalInterval-(long)(mediaPrepareItem*currentItem);
				float speedOfData= ((float)currentSize)/((float)intervalOfSize);
				float speedOfPrep= ((float)currentItem)/mediaPrepareItem;
				long eta= (long) ((totalSizeOfItems-currentSize)/speedOfData +
						  (totalNumberOfItems-currentItem)/speedOfPrep);
				show=buildETAELA(show, totalInterval, eta);
			}
		}
		System.out.print("\r"+show);
		lastAdvCall=now;
		return true;
	}

	private String advanceMessage(long now, String msg, String show, String rightMsg) {
		long totalInterval=now-initProcess;
		// speed of process [units per milisecond]
		float speedOfData= ((float)currentSize)/((float)totalInterval);
		if(totalSizeOfItems==0){
			String speed=new DecimalFormat("#,##0").format(speedOfData*1000);
			show+=String.format("At %s itm/seg ",speed);
		}else{
			// ETA Estimated time abend (milis)
			long eta = (long)((float)(totalSizeOfItems-currentSize)/speedOfData);
			show = buildETAELA(show, totalInterval, eta);
		}
		return formatMessage(msg, show, rightMsg);
	}

	private String buildETAELA(String show, long totalInterval, long eta) {
		//% avance
		float pcf=((float)currentSize)*100F/((float)totalSizeOfItems);
		long  pcl=(long)pcf;
		Sexagesimal stim=new Sexagesimal(eta);
		Sexagesimal elap=new Sexagesimal(totalInterval);
		show+=String.format("%02d%% ELA %d:%02d:%02d ETA %d:%02d:%02d ", 
				pcl,elap.h,elap.m,elap.s,stim.h,stim.m,stim.s);
		return show;
	}

	private String formatMessage(String msg, String show, String rightMsg) {
		if(show.length()+staticMsg.length()+1+msg.length()>consoleLineSize-1){
			int rest=consoleLineSize-show.length()-staticMsg.length()-2;
			if(rest>msg.length())
				rest=msg.length();
			show=staticMsg+" "+show+msg.substring(msg.length()-rest);
		}else{
			show=staticMsg+" "+show+msg+spaces;
		}
		while(show.length()<consoleLineSize-1)
			show+=spaces;
		String left=show.substring(0,consoleLineSize-1);
		while(rightMsg.length()>left.length())
			rightMsg=rightMsg.substring(2);
		show=left.substring(0,consoleLineSize-rightMsg.length()-1)+rightMsg;
		return show;
	}

	private String estimatingMessage() {
		String show;
		estimatePos+=estimatePosInc;
		if(estimatePos>=3 || estimatePos<0)
			estimatePosInc=-estimatePosInc;
		if(estimatePos<0)
			estimatePos=1;
		
		show=points.substring(0,estimatePos)+"Estimating--- ";
		show=show.substring(0,13)+" ";
		return show;
	}

	class Sexagesimal {
		public long h,m,s;
		public Sexagesimal(long milis){
			h= milis/3600000L;
			m=(milis/1000-h*3600)/60;
			s= milis/1000-h*3600-m*60;
		}
	}
	
	public long  getCurrentValue(){
		return currentSize;
	}
	
	public long getCurrentItem(){
		return currentItem;
	}
	
	public void finish(){
		while(!reportAdvance(0, "Done"))
			;
	}
}
