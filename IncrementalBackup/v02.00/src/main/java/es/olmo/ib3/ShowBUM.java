package es.olmo.ib3;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

public class ShowBUM {

	public static void main(String[] args) {
		if(args.length==0){
			System.out.println("Show path_to_stat.gz");
			System.exit(0);
		}
		try {
			for(String relPath:args){
				File in=new File(relPath);
				FileInputStream fis=new FileInputStream(in);
				GzipCompressorInputStream gzis=new GzipCompressorInputStream(fis);
				ObjectInputStream ois=new ObjectInputStream(gzis);
				String name=(String)ois.readObject();
				Snapshot snap=(Snapshot)ois.readObject();
				snap.root.setName(name);
				snap.root.resetStatus(0, Nodo.opMatch, false, null, true);
				snap.root.showWalkerTree(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
