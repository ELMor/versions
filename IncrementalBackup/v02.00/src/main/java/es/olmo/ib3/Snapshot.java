package es.olmo.ib3;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;

import de.schlichtherle.truezip.zip.ZipEntry;
import de.schlichtherle.truezip.zip.ZipFile;
import de.schlichtherle.truezip.zip.ZipOutputStream;

public class Snapshot implements Serializable {
	private static final long serialVersionUID = -2766258539780736578L;
	private transient int created=0,updated=0,deleted=0;
	transient BackupManager bum;
	private String zipRef;
	Nodo root=new Nodo("");
	
	public Snapshot(BackupManager bum){
		this.bum=bum;
	}
	
	@Override
	public String toString(){
		return "Snapshot:"+root.toString();
	}
	
	public Snapshot(File snpf){
		Progress p=new Progress("Stating files", 0, 0, 80);
		root=new Nodo(snpf,p);
		showInfo();
	}

	public Snapshot(ZipFile zf, Progress p){
		root=new Nodo(zf,p);
		p.finish();
		showInfo();
	}

	private void showInfo() {
		String nDirs=new DecimalFormat("#,##0").format(root.getNumberOfDirs());
		String nFils=new DecimalFormat("#,##0").format(root.getNumberOfFiles(Nodo.opAll));
		System.out.printf("\nFound %s Dirs and %s files\n",nDirs,nFils);
	}
	
	public void diff(Snapshot newer){
		created=updated=deleted=0;
		buffer=new byte[4194304];
		//Mark childs as deleted and untouched
		for(Nodo olderChild:this.root)
			olderChild.resetStatus(0,Nodo.opDeleted,false,null,true);
		for(Nodo newerChild:newer.root)
			newerChild.resetStatus(0,Nodo.opUntouched,false,null,true);
		System.out.print("Performing diff..");
		zipRef=RootBackupManager.getCurrentDate();
		diff(root,newer.root);
		System.out.printf("%d news, %d updated, %d deleted\n", created,updated,deleted);
	}
	
	public void diff(Nodo esteDir, Nodo otroDir){
		for(Nodo remChild:otroDir){
			long fech=remChild.getTime();
			Nodo local=esteDir.getChildWithSameName(remChild);
			if(local==null){ //Nuevo
				//Añadimos el nodo al local
				esteDir.addHijo(remChild);
				remChild.resetStatus(fech,Nodo.opCreated,true,zipRef,true);
				created++;
			}else{
				local.resetStatus(0,Nodo.opUntouched,false,null,false);//Not deleted
				if(local.isDir() && remChild.isDir()){
					diff(local,remChild);
				}else if(local.isDir() || remChild.isDir()){
					//Hmm, suppose local is deleted and created as a file/dir when was dir/file
					esteDir.addHijo(remChild);
					remChild.resetStatus(fech,Nodo.opCreated,true,zipRef,true);
					created++;
				}else if(local.getTime()<remChild.getTime()){//Updated or re-created
					if(local.isDeleted()){
						local.resetStatus(fech,Nodo.opCreated,true,zipRef,false);
						local.setTFRef(remChild.getFileRef());
						local.setTime(remChild.getTime());
						created++;
					}else{
						local.resetStatus(fech,Nodo.opUpdated,true,zipRef,false);
						local.setTFRef(remChild.getFileRef());
						local.setTime(remChild.getTime());
						updated++;
					}
				}
			}
		}
		//Quedan marcados como opDeleted los nodos que no han sido encontrados
		for(Nodo prev:esteDir){
			if(prev.getStatus()==Nodo.opDeleted){
				deleted++;
				prev.setTime(System.currentTimeMillis());//Date of delete is unknown
				prev.resetStatus(System.currentTimeMillis(),Nodo.opDeleted, true, zipRef,false);
			}
		}
	}
	
	public void commit(ZipOutputStream zf, Progress p) throws Exception{
		for(Nodo child:root)
			commit(child,zf,p);
		p.finish();
	}
	
	public boolean commit(Nodo colored, ZipOutputStream zf,Progress p) throws Exception{
		if(colored.isDir()){
			for(Nodo child:colored)
				commit(child,zf,p);
			return true;
		}else{
			switch(colored.getStatus()){
			case Nodo.opCreated: 
			case Nodo.opUpdated:
				return save(colored,zf,p);
			case Nodo.opDeleted:
				return delete(colored,zf,p);
			}
		}
		return true;
	}
	
	public void extract(ExtractManager xt) throws Exception{
		for(Nodo child:root)
			extract(child,xt);
		xt.finishExtract();
	}
	
	public transient ZipFile openedZipFile;
	public transient String openedZipFileName="Noopened zip file name s registerd";
	public void extract(Nodo colored, ExtractManager xt) throws Exception{
		if(colored.isDeleted())
			return;
		if(colored.isDir()){
			for(Nodo child:colored)
				extract(child,xt);
		}else{
			String zfn=colored.getSelZip()+RootBackupManager.snapExt;
			if(openedZipFileName==null || !openedZipFileName.equals(zfn)){
				if(openedZipFile!=null)
					openedZipFile.close();
				File nf=bum.getFileDir();
				nf=new File(nf,zfn);
				openedZipFile=new ZipFile(nf);
				openedZipFileName=zfn;
			}
			String relPath=colored.getRelativeName();
			InputStream is=openedZipFile.getInputStream(relPath);
			try {
				xt.itemExtract(relPath,is,colored.getName());
			} catch (Exception e) {
				System.err.println("\rError reading "+relPath+" from "+openedZipFileName);
			}
		}
	}
	
	public static final String delMark="<DELETED>";
	private boolean delete(Nodo colored, ZipOutputStream zf,Progress prog) throws Exception{
		String relName=colored.getRelativeName();
		ZipEntry ze=new ZipEntry(relName+delMark);
		ze.setTime(colored.getTime());
		zf.putNextEntry(ze);
		colored.setDeleted(true);
		return true;
	}
	
	private transient byte buffer[]=null;
	public boolean save(Nodo nodo, ZipOutputStream zf, Progress prog) throws IOException {
		//start preparing item
		File in;
		FileInputStream fis;
		in = nodo.getFileRef();
		try {
			fis = new FileInputStream(in);
			//prog.startPrepareItem();
			String relName=nodo.getRelativeName();
			ZipEntry entry=new ZipEntry(relName);
			long nt=nodo.getTime();
			entry.setTime(nt);//No tiene la precision suficiente. Hay que grabarlo en extra.
			entry.setComment(Long.toString(nt, 36));
			zf.setLevel(CmpExt.getCmpLevel(relName));
			zf.putNextEntry(entry);
			int readed;
			//End prepare item
			//prog.endPrepareItem();
			while((readed=fis.read(buffer))>=0){
				zf.write(buffer, 0, readed);
				//report size advance
				prog.reportAdvance(readed, nodo.getFullName());
			}
		} catch (Exception e) {
			System.err.println("\nCannot open "+in.getAbsolutePath()+". It WILL NOT BE Backed up");
			return false;
		}
		fis.close();
		return true;
	}

	public String getZipRef() {
		return zipRef;
	}

	public static String readableFileSize(long size) {
	    if(size <= 0) return "0";
	    final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    return new DecimalFormat("#,##0.##").format(size/Math.pow(1024, digitGroups)) + units[digitGroups];
	}
	
	public long getTotalSize(byte mask){
		return root.getSize(mask);
	}
	
	public long getTotalNumber(byte mask){
		return root.getNumberOfFiles(mask);
	}
}
