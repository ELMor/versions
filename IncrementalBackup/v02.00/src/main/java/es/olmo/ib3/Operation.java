package es.olmo.ib3;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Vector;

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

public class Operation {
	RootBackupManager rbuManager;
	Vector<String> fileSpecs;
	String rootOfSource;
	String dateOfOperation;
	
	public Operation(RootBackupManager rbum, String rSrc, Vector<String> spec) throws Exception{
		rbuManager=rbum;
		fileSpecs=spec;
		rootOfSource=rSrc;
	}
	public void update() throws Exception{
		if(fileSpecs.size()==0){
			File ros=new File(rootOfSource);
			int rosLen=ros.getCanonicalPath().length();
			File list[]=IncBackupProp.filter(ros);
			for(File ex:list){
				if(!ex.isDirectory())
					continue;
				String rPath=ex.getCanonicalPath().substring(rosLen);
				fileSpecs.add(rPath);
			}
		}
		for(String relPath:fileSpecs){
			System.out.print("\nUpdate on "+relPath+"...\n");
			BackupManager bum=initBUM("For Update of ", relPath);
			//Build File System snapshot
			Snapshot other=new Snapshot(new File(rootOfSource,relPath));
			bum.diff(other);
			bum.commit();
		}
	}
	public void list() throws Exception{
		if(fileSpecs.size()==0)
			fileSpecs.add("*");
		for(String relPath:fileSpecs){
			System.out.println("List of "+relPath);
			for(BackupManager bum:rbuManager){
				bum.match(relPath,null,true);
				System.out.println("  Checking "+bum.getRelPath());
				bum.showMatched();
			}
		}
	}
	/**
	 * Extract files
	 * @param fileDest may be null.
	 * @throws Exception 
	 */
	public void extract(String dateSelected, String fileDest) throws Exception{
		if(fileDest==null)
			fileDest=".";
		long totalItems=0;
		long totalSize=0;
		for(BackupManager bum:rbuManager){
			Snapshot currSnap=bum.getCurrentSnapshot();
			currSnap.root.resetStatus(0, Nodo.opUntouched, false, null, true);
			for(String relPath:fileSpecs){
				bum.match(relPath,dateSelected,true);
				totalItems+=currSnap.getTotalNumber(Nodo.opMatch);
				totalSize+=currSnap.getTotalSize(Nodo.opMatch);
			}
		}
		String msgs=String.format("Extracting %s",Snapshot.readableFileSize(totalSize));
		Progress p=new Progress(msgs, totalItems, totalSize, 80);
		ExtractManager xt;
		if(fileDest.toLowerCase().endsWith(".zip")){
			xt=new ZipExtractDestination(p, fileDest);
		}else{
			xt=new FileExtractDestination(new File(fileDest), p);
		}
		for(BackupManager bum:rbuManager)
			bum.extract(xt);
	}
	
	public void portable(String fileDest) throws Exception{
		if(fileDest==null)
			fileDest="Portable-"+RootBackupManager.getCurrentDate()+".zip";
		Progress prog=new Progress("Creating "+fileDest, Progress.szUnknown, Progress.szUnknown, 80);
		ExtractManager xt=new ZipExtractDestination(prog, fileDest);
		for(BackupManager bum:rbuManager){
			File stat=bum.getStatFile();
			String rp=bum.getRelPath();
			xt.itemExtract(rp+File.separator+RootBackupManager.statName, 
					new FileInputStream(stat), rp);
		}
		xt.finishExtract();
	}
	public void stat() throws Exception{
		//Find all STAT.gz under rootOfbackup and delete
		rbuManager.clearSTATs();
		for(BackupManager bum:rbuManager){
			System.out.println("Re-stating "+bum);
			bum.createSaveAndLoadStat();
		}
	}
	public void showStat() throws Exception{
		for(String relPath:fileSpecs){
			File in=new File(relPath);
			FileInputStream fis=new FileInputStream(in);
			GzipCompressorInputStream gzis=new GzipCompressorInputStream(fis);
			ObjectInputStream ois=new ObjectInputStream(gzis);
			String name=(String)ois.readObject();
			Snapshot snap=(Snapshot)ois.readObject();
			snap.root.setName(name);
			snap.root.resetStatus(0, Nodo.opMatch, false, null, true);
			snap.root.showWalkerTree(0);
		}
	}
	
	private BackupManager initBUM(String logTxt, String relPath) throws Exception {
		BackupManager bum=rbuManager.getBUM(relPath);
		if(bum==null){
			bum=rbuManager.createBUM(relPath);
			rbuManager.log("Creating ",relPath);
		}
		chkBUM(bum, rootOfSource);
		rbuManager.log(logTxt,relPath);
		return bum;
	}
	private void chkBUM(BackupManager bum, String relPath){
		if(bum==null)
			throw new RuntimeException(relPath+" is not a backup location.");
	}
}
