package es.olmo.ib3;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class IncBackupProp implements FileFilter {
	Properties prop;
	Vector<Pattern> includes;
	Vector<Pattern> excludes;
	
	public IncBackupProp(Properties p){
		prop=p;
		String includeMatch=prop.getProperty("Include");
		String excludeMatch=prop.getProperty("Exclude");
		includes=tokenPart(includeMatch,false);
		excludes=tokenPart(excludeMatch,false);
	}
	
	public static File[] filter(File dir) {
		File pr=new File(dir,".IncBackup.properties");
		if(!pr.exists()){
			return dir.listFiles();
		}
		try {
			Properties prop=new Properties();
			FileInputStream fis=new FileInputStream(pr);
			prop.load(fis);
			fis.close();
			if(prop.getProperty("Ignore","false").equalsIgnoreCase("true"))
				return new File[0];
			return dir.listFiles(new IncBackupProp(prop));
		} catch (Exception e) {
		}
		return dir.listFiles();
	}
	
	public static Pattern substitutePatternChars(String pattern, boolean wideMatch) {
		Pattern pat;
		pattern=pattern.replace("__ALLFILES__", "*");
		pattern=pattern.replace(".", "\\.");
		pattern=pattern.replace("$", "\\$");
		pattern=pattern.replace("?", ".");
		pattern=pattern.replace("*", ".*");
		if(wideMatch)
			pattern=".*"+pattern+".*";
		pat=Pattern.compile(pattern);
		return pat;
	}
	
	private Vector<Pattern> tokenPart(String lst,boolean wideMatch){
		Vector<Pattern> toret=new Vector<>();
		if(lst==null)
			return toret;
		Vector<String> strs=new Vector<>();
		StringTokenizer st=new StringTokenizer(lst,";:");
		while(st.hasMoreElements()){
			strs.add(st.nextToken());
		}
		for(String s:strs)
			toret.add(	substitutePatternChars(s,wideMatch));
		return toret;
	}
	
	@Override
	public boolean accept(File file) {
		String fname=file.getName();
		//Includes
		for(Pattern p:includes){
			Matcher m=p.matcher(fname);
			if(m.matches())
				return true;
		}
		for(Pattern p:excludes){
			Matcher m=p.matcher(fname);
			if(m.matches())
				return false;
		}
		if(includes.size()>0)
			return false;
		if(excludes.size()>0)
			return true;
		return true;
	}
	
}
