package es.olmo.ib3;

import java.util.Hashtable;

public class CmpExt {
	public static final Hashtable<String, Integer> tbl=new Hashtable<>();
	static {
		tbl.put(".mp3" ,1); tbl.put(".tgz" ,1); tbl.put(".avi" ,1);
		tbl.put(".ogg" ,1); tbl.put(".tbz" ,1); tbl.put(".mp4" ,1);
		tbl.put(".wma" ,1); tbl.put(".rar" ,1); tbl.put(".mkv" ,1);
		tbl.put(".zip" ,1); tbl.put(".7z"  ,1); tbl.put(".wmv" ,1);
		tbl.put(".png" ,1); tbl.put(".cbr" ,1); tbl.put(".m3a" ,1);
		tbl.put(".jpeg",1); tbl.put(".cbz" ,1); tbl.put(".apk" ,1);
		tbl.put(".epub",1); tbl.put(".gz"  ,1); tbl.put(".pak" ,1);
		tbl.put(".m4a" ,1); tbl.put(".jpg" ,1); tbl.put(".ipa" ,1);
		tbl.put(".txt" ,9); tbl.put(".xls" ,5); tbl.put(".pptx",1);
		tbl.put(".htm" ,9); tbl.put(".ppt" ,5); tbl.put(".m4v" ,1);
		tbl.put(".html",9); tbl.put(".docx",1); tbl.put(".pst" ,9);
		tbl.put(".doc" ,5); tbl.put(".xlsx",1);
	}

	public static final int defaultCmpLevel=5;
	public static final int defaultNoCmpLevel=1;
	
	public static int getCmpLevel(String file){
		String lw=file.toLowerCase();
		int ndx=lw.lastIndexOf('.');
		if(ndx<0)
			return defaultCmpLevel;
		String ext=lw.substring(ndx);
		Integer ret=tbl.get(ext);
		if(ret==null)
			return defaultCmpLevel;
		return ret.intValue();
	}
	
}
