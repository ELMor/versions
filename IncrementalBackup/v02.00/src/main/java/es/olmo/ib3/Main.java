package es.olmo.ib3;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * 
 * @author elinares
 *
 */

public class Main {
	
	public static final boolean debug=false;
	
	static String rootOfBackup=null;
	static String dateSelected=null;
	static String rootOfSource=null;
	static String destination=null; 
	static Getopt g=null;
	static int opMode=0; // 1 to update, 2 to list, 3 to extract, 4 to stat, 5 get portable stats

	/**
	 * 
	 * @param c
	 */
	public static void singleCharOpt(char c){
		switch(c){
		case 'h': printUsageAndExit();	
		case 'b': rootOfBackup=g.getOptarg(); break;
		case 'r': rootOfSource=g.getOptarg(); break;
		case 't': dateSelected=g.getOptarg(); break;
		case 'd': destination=g.getOptarg(); break;
		case 'u': setMode(1); break;
		case 'l': setMode(2); break;
		case 'x': setMode(3); break;
		case 's': setMode(4); break;
		case 'p': setMode(5); break;
		case 'z': setMode(6); break;
		case 'w': setMode(7); break;
		case '?': System.exit(-1); break;
		}
	}
	
	public static void main(String args[]){
		Vector<String> fileSpecs=new Vector<>();
		LongOpt opts[]=new LongOpt[12];
		StringBuffer sb=new StringBuffer();

		opts[ 0]=new LongOpt("help"        , LongOpt.NO_ARGUMENT,     null, 'h');
		opts[ 1]=new LongOpt("rootOfBackup", LongOpt.REQUIRED_ARGUMENT, sb, 'b');
		opts[ 3]=new LongOpt("update"            , LongOpt.NO_ARGUMENT, sb, 'u');
		opts[ 4]=new LongOpt("list"              , LongOpt.NO_ARGUMENT, sb, 'l');
		opts[ 5]=new LongOpt("extract"           , LongOpt.NO_ARGUMENT, sb, 'x');
		opts[ 6]=new LongOpt("stat"              , LongOpt.NO_ARGUMENT, sb, 's');
		opts[ 7]=new LongOpt("portable"          , LongOpt.NO_ARGUMENT, sb, 'p');
		opts[ 8]=new LongOpt("time"        , LongOpt.OPTIONAL_ARGUMENT, sb, 't');
		opts[ 9]=new LongOpt("rootOfSrc"   , LongOpt.OPTIONAL_ARGUMENT, sb, 'r');
		opts[10]=new LongOpt("dest"        , LongOpt.OPTIONAL_ARGUMENT, sb, 'd');
		opts[11]=new LongOpt("showstat"    , LongOpt.OPTIONAL_ARGUMENT, sb, 'w');
		
		g=new Getopt("ib", args,"-:hb:ulxspwt:r:d:" ,opts);
		int c;
		while((c=g.getopt())!=-1){
			switch(c){
			case 0: //long option
				char i=(char)(new Integer(sb.toString())).intValue();
				singleCharOpt(i);
				break;
			case 1:
				fileSpecs.add(g.getOptarg());
				break;
			default :
				singleCharOpt((char)c);
				break;
			}
		}
		int ndxOri=g.getOptind();
		if(ndxOri<args.length){
			for(int i=ndxOri;i<args.length;i++)
				fileSpecs.add(args[i]);
			
		}else if( fileSpecs.size()==0 && opMode==3){
			System.out.println("You must specify at least one dir");
			System.exit(-1);
		}
		File rootOfBackupFile=null;
		RootBackupManager rbum=null;
		if(rootOfBackup==null && opMode!=7){
			System.out.println("You must specify -b dir");
			System.exit(-1);
		}
		if(rootOfBackup!=null){
			rootOfBackupFile=new File(rootOfBackup);
			rbum=new RootBackupManager(rootOfBackupFile);
		}
		try {
			Operation op=new Operation(rbum, rootOfSource, fileSpecs);
			switch(opMode){
			case 0: System.out.println("No operation selected");
				    printUsageAndExit();
			case 1: op.update();             
					break;
			case 2: op.list();               
					break;
			case 3: op.extract(dateSelected, destination); 
					break;
			case 4: op.stat();               
					break; 
			case 5: op.portable(destination);           
					break;
			case 7: op.showStat();
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void setMode(int m){
		if(opMode==0){
			opMode=m;
		}else{
			System.out.println("Only one operation is allowed.");
			System.exit(-1);
		}
	}

	private static void printUsageAndExit(){
		Class<? extends Main> cls= new Main().getClass();
		InputStream is=cls.getResourceAsStream("Readme.txt");
		InputStreamReader isr=new InputStreamReader(is);
		BufferedReader br=new BufferedReader(isr);
		String linea;
		try {
			while((linea=br.readLine())!=null)
				System.out.println(linea);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}
}
