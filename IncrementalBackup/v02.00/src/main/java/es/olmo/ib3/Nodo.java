package es.olmo.ib3;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.schlichtherle.truezip.zip.ZipEntry;
import de.schlichtherle.truezip.zip.ZipFile;

public class Nodo implements Serializable, Comparable<Nodo>, Iterable<Nodo> {
	private static final long serialVersionUID = 1709310092884694300L;

	private Nodo parent=null;
	private Vector<Nodo> childs=new Vector<>();
	
	private String nombre="";
	private long date;
	private long size=0;
	private boolean isDeleted=false;
	private boolean dir;
	private byte status=1; 
	public History history=new History();

	private transient File fileRef=null;

	@Override
	public String toString(){
		return getFullName() + (isDir()?"/":"");
	}
	
	public Nodo(String name){
		dir=true;
		nombre=name;
	}
	
	public Nodo(File file, Progress p){
		fileRef=file;
		nombre=file.getName();
		if(file.isDirectory()){
			setTime(0);
			dir=true;
			File list[] = IncBackupProp.filter(file) ;
			if(list!=null)
				for(File child:list)
					new Nodo(child,this,p);
			size=0;
		}else{
			date=file.lastModified();
			size=file.length();
			dir=false;
			p.reportAdvance(1,file.getAbsolutePath());
		}
	}
	
	public Nodo(ZipFile zf,Progress p){
		dir=true;
		Enumeration<? extends ZipEntry> entries=zf.entries();
		String zfName=new File(zf.getName()).getName();
		zfName=zfName.substring(0,zfName.length()-RootBackupManager.snapExt.length());
		while(entries.hasMoreElements()){
			ZipEntry ze=entries.nextElement();
			String fullName=ze.getName();
			if(fullName.endsWith(Snapshot.delMark)){
				continue; //omit it
			}
			int ndx=beginOfFileName(fullName);
			String path=ndx<0?"":fullName.substring(0, ndx-1);
			String name=ndx<0?fullName:fullName.substring(ndx);
			Nodo nuevo=new Nodo(name);
			nuevo.dir=false;
			nuevo.setTime(ze);
			nuevo.size=ze.getSize();
			nuevo.resetStatus(nuevo.date, Nodo.opCreated, true, zfName, false);
			if(ndx<0)
				addHijo(nuevo);
			else
				mkdirs(path).addHijo(nuevo);
			p.reportAdvance(1, fullName);
		}
	}
	
	private void setTime(ZipEntry ze){
		String hs=ze.getComment();
		if(hs!=null){
			try {
				date=Long.parseLong(hs, 36);
			}catch(NumberFormatException e){
				date=ze.getTime();
			}
		}else{
			date=ze.getTime();
		}
	}
	
	public void registerSnap(ZipFile zf, Progress p){
		Enumeration<? extends ZipEntry> entries=zf.entries();
		String zfName=new File(zf.getName()).getName();
		zfName=zfName.substring(0,zfName.length()-RootBackupManager.snapExt.length());
		while(entries.hasMoreElements()){
			boolean deleteOp=false;
			ZipEntry ze=entries.nextElement();
			String fullName=ze.getName();
			if(fullName.endsWith(Snapshot.delMark)){
				deleteOp=true;
				fullName=fullName.substring(0,fullName.length()-Snapshot.delMark.length());
			}
			int ndx=beginOfFileName(fullName);
			String path=ndx<0?"":fullName.substring(0, ndx-1);
			String name=ndx<0?fullName:fullName.substring(ndx);
			Nodo padre;
			if(path.equals(""))
				padre=this;
			else
			    padre=mkdirs(path);
			Nodo hijode=padre.getChildWithSameName(new Nodo(name));
			byte newStatus;
			if(hijode!=null){
				if(deleteOp){//Deleted
					newStatus=Nodo.opDeleted;
				}else{//Updated
					newStatus=Nodo.opUpdated;
				}
			}else{//Created
				newStatus=Nodo.opCreated;
				hijode=new Nodo(name);
				padre.addHijo(hijode);
			}
			hijode.dir=false;
			hijode.setTime(ze);
			hijode.size=ze.getSize();
			hijode.resetStatus(hijode.date, newStatus, true, zfName, false);
			p.reportAdvance(1, fullName);
		}
	}
	
	private Nodo mkdirs(String path){
		int ndx=path.indexOf('\\');
		ndx=ndx<0?path.indexOf('/'):ndx;
		String pre=ndx<0?path:path.substring(0, ndx);
		String pos=ndx<0?null:path.substring(ndx+1);
		int ndxHijo=Collections.binarySearch(childs, new Nodo(pre));
		Nodo hijo;
		if(ndxHijo>=0){ //Ya existe
			hijo=childs.elementAt(ndxHijo);
		}else{ //Hay que crearlo
			hijo=new Nodo(pre);
			addHijo(hijo);
		}
		if(pos==null) //Es el ultimo del path
			return hijo;
		return hijo.mkdirs(pos);
	}
	
	private int beginOfFileName(String s){
		int ndx1=s.lastIndexOf('\\');
		if(ndx1<0)
			ndx1=s.lastIndexOf('/');
		if(ndx1<0)
			return -1;
		return ndx1+1;
	}
	
	private Nodo(File child, Nodo parent, Progress p){
		this(child,p);
		parent.addHijo(this);
	}
	
	public void addHijo(Nodo child){
		dir=true;
		int pos=Collections.binarySearch(childs, child);
		if(pos<0){
			childs.insertElementAt(child, -pos-1);
		}else{
			if(pos==childs.size()){
				childs.add(child);
			}else{
				childs.setElementAt(child, pos);
			}
		}
		child.parent=this;
	}
	
	public Nodo getChildWithSameName(Nodo other){
		int pos=Collections.binarySearch(childs, other);
		if(pos<0)
			return null;
		return childs.elementAt(pos);
	}
		
	public static final byte opUntouched=1;
	public static final byte opCreated=2;
	public static final byte opUpdated=4;
	public static final byte opDeleted=8;
	public static final byte opMatch=16;
	public static final byte opAll=31;
	
	class HistoryAction implements Serializable {
		private static final long serialVersionUID = 1L;
		int typeOfAction=0; // 1: Added, 2: Updated, 3:deleted
		long date=0; //Time of operation
		String zipRef;
		public HistoryAction(int tipo, long fecha, String zr){
			typeOfAction=tipo;
			date=fecha;
			zipRef=zr;
		}
	}
	
	class History implements Serializable{
		private static final long serialVersionUID = 1L;
		Vector<HistoryAction> ops=new Vector<>();
		int selectedZipNumber;
		public void register(int op, long date, String zr){
			ops.add(new HistoryAction(op,date,zr));
		}
		public int whichZip(String zr){
			if(zr==null)
				return ops.size()-1;
			return whichZip(parseDate(zr));
		}
		public int whichZip(long date){
			long lastOpDate=ops.get(0).date;
			for(int i=0;i<ops.size();i++){
				HistoryAction ha=ops.elementAt(i);
				if(ha.date==date)
					return i;
				if(lastOpDate<date&&date<ha.date)
					return i;
				lastOpDate=ha.date;
			}
			return ops.size()-1;
		}
		public void selectZip(int zr){
			selectedZipNumber=whichZip(zr);
		}
		public void selectZip(long date){
			selectedZipNumber=whichZip(date);
		}
	}

	@Override
	public int compareTo(Nodo o) {
		return nombre.compareTo(o.nombre);
	}

	@Override
	public Iterator<Nodo> iterator() {
		return childs.iterator();
	}

	public boolean isDir() {
		return dir;
	}

	public String getName(){
		return nombre;
	}
	
	public String getFullName(){
		String ret=nombre;
		for(Nodo p=parent;p!=null;p=p.parent)
			ret=p.nombre+"/"+ret;
		return ret;
	}
	
	public String getRelativeName(){
		if(parent==null)
			return "";
		String pre=parent.getRelativeName();
		if(pre.equals(""))
			return nombre;
		else
			return pre+File.separator+nombre;
	}
	
	/**
	 * Change status of this node and its children
	 * @param status New status (see op---)
	 * @param registerHistory If true, append an entry to history
	 * @param zipRef Name of the snapshot that holds the version
	 */
	public void resetStatus(long fecha, byte status, boolean registerHistory, String zipRef, boolean includeChildren){
		this.status=status;
		if(registerHistory && status!=opUntouched){
			history.register(status,fecha,zipRef);
		}
		if(isDir() && includeChildren)
			for(Nodo child:childs)
				child.resetStatus(fecha,status,registerHistory,zipRef,includeChildren);
	}
	/**
	 * Calculate th sum of the sizes
	 * @param mask select only status that mask with it
	 * @return total size
	 */
	public long getSize(byte mask){
		boolean select=(status & mask)!=0;
		long ret= select ? size : 0;
		if(isDir()){
			ret=0;
			for(Nodo child:childs)
				ret+=child.getSize(mask);
		}
		return ret;
	}
	
	/**
	 * Get number of sleeves 
	 * @param mask
	 * @return
	 */
	public long getNumberOfFiles(byte mask){
		if((status & mask)!=0){
			long ret=0;
			if(isDir()){
				for(Nodo child:childs){
					if(child.isDir())
						ret+=child.getNumberOfFiles(mask);
					else
						ret++;
				}
				return ret;
			}else{
				return 1;
			}
		} else {
			return 0;
		}
	}
	
	/**
	 * Get number of dirs
	 * @return
	 */
	public long getNumberOfDirs(){
		long ret= 0 ;
		if(isDir()){
			for(Nodo child:childs)
				ret+=(child.isDir()?1:0)+child.getNumberOfDirs();
		}
		return ret;
	}

	public int getStatus() {
		return status;
	}


	public long getTime() {
		return date;
	}

	public void setTime(long date) {
		this.date = date;
	}
	
	public void setTFRef(File r){
		fileRef=r;
	}
	
	public File getFileRef(){
		return fileRef;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public void matchWalker(Pattern pat,String dsel){
		String tom=getFullName();
		Matcher matcher=pat.matcher(tom);
		if(isDir()){
			if(matcher.matches())
				status=opMatch;
			for(Nodo child:childs)
				child.matchWalker(pat,dsel);
		}else{
			if(matcher.matches()){
				status=opMatch;
				history.whichZip(dsel);
			}
		}
	}
	/**
	 * Show a filetree like picture
	 * @param depth of current dir
	 */
	public void showWalkerTree(int depth){
		if(isAnyChildMatched()){
			for(int i=0;i<depth;i++)
				System.out.print("|  ");
			String nameOfFile=(nombre==null||nombre.length()==0 ? "Backup":nombre);
			if(isDir())
				nameOfFile+=File.separator;
			System.out.println("+--"+ nameOfFile);
			for(Nodo child:childs)
				if(child.isDir())
					child.showWalkerTree(depth+1);
			for(Nodo child:childs){
				if(!child.isDir() && child.status==opMatch){
					Vector<String> lines=child.listEntryHistory();
					for(int i=0;i<lines.size();i++){
						for(int j=0;j<=depth;j++){
							System.out.print("|  ");
						}
						if(i==0)
							System.out.println("+--"  +lines.get(i));
						else if(i==1)
							System.out.println(" \\  "+lines.get(i));
						else
							System.out.println("  | " +lines.get(i));
					}
				}
			}
		}
	}
	
	private boolean isAnyChildMatched(){
		if(status==opMatch)
			return true;
		boolean toRet=false;
		for(Nodo child:childs){
			if(toRet)
				break;
			else
				toRet|=child.isAnyChildMatched();
		}
		return toRet;
	}
	
	public Vector<String> listEntryHistory(){
		Vector<String> ret=new Vector<>();
		ret.add(nombre + (isDir() ? File.separator : ""));
		SimpleDateFormat sdf=new SimpleDateFormat();
		sdf.applyPattern(RootBackupManager.dateFormat);
		for(HistoryAction ha:history.ops){
			String d=sdf.format(new Date(ha.date));
			switch(ha.typeOfAction){
			case opCreated: ret.add(ha.zipRef+RootBackupManager.snapExt+": Created "+d); break;
			case opUpdated: ret.add(ha.zipRef+RootBackupManager.snapExt+": Updated "+d); break;
			case opDeleted: ret.add(ha.zipRef+RootBackupManager.snapExt+": Deleted "); break;
			}
		}
		return ret;
	}

	public void setName(String n) {
		nombre=n;
	}

	public static long parseDate(String s){
		long ret=0;
		try {
			SimpleDateFormat sdf=new SimpleDateFormat(RootBackupManager.dateFormat);
			Date d=sdf.parse(s);
			ret=d.getTime();
		}catch(Exception e){
			return 0;
		}
		return ret;
	}	
	public String getSelZip(){
		return history.ops.elementAt(history.selectedZipNumber).zipRef;
	}
	
}
